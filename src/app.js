const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
// const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
// const winston = require('winston');

const feathers = require('@feathersjs/feathers');
const express = require('@feathersjs/express');
const logger = require('feathers-logger');
const configuration = require('@feathersjs/configuration');
const rest = require('@feathersjs/express/rest');
const restClient = require('@feathersjs/rest-client');
const socketio = require('@feathersjs/socketio');

const handler = require('@feathersjs/express/errors');
const notFound = require('feathers-errors/not-found');

const middleware = require('./middleware');
const services = require('./services');
const channels = require('./channels');
const appHooks = require('./app.hooks');

const mongoose = require('./mongoose');

const authentication = require('./authentication');

const axios = require('axios');

const app = express(feathers());

// Load app configuration
app.configure(configuration());
// Enable CORS, security, compression, favicon and body parsing
// app.options('/*', cors());
// app.use(cors());
app.use(helmet());
app.use(compress());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', express.static(app.get('public')));

// Set up logger
app.configure(logger());

app.configure(mongoose);
app.configure(rest());
app.configure(socketio({
  wsEngine: 'ws', // alt uws, needs uws package
  path: '/ws/'
}, function(io) {
  io.on('connection', function(socket) {
    socket.emit('custom event', {data: 'chao is awesome!'});
    // socket.on('create', function(collection, payload) {
    //   console.log(collection);
    //   console.log(payload);
    // });
  });

  // Registering Socket.io middleware
  io.use(function (socket, next) {
    // Exposing a request property to services and hooks
    socket.feathers.referrer = socket.request.referrer;
    next();
  });
}));

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up socket event channels
app.configure(channels);

// Configure rest client to use axios
app.configure(restClient().axios(axios));

// Configure a middleware for 404s and the error handler
app.use(notFound());
app.use(handler());

app.hooks(appHooks);

module.exports = app;
