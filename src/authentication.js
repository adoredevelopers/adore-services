const authentication = require('@feathersjs/authentication');
const jwt = require('@feathersjs/authentication-jwt');
const local = require('@feathersjs/authentication-local');
const oauth2 = require('@feathersjs/authentication-oauth2');
// const LinkedinStrategy = require('passport-linkedin-oauth2').Strategy;
const LinkedinStrategy = require('passport-linkedin-oauth2').Strategy;
const FacebookStrategy = require('passport-facebook');
const GithubStrategy = require('passport-github');

// const OAuth2Verifier = require('./middleware/oauth2.verifier');
const OAuth2Handler = require('./middleware/oauth2.handler');

module.exports = function () {
  const app = this;
  const config = app.get('authentication');

  // Set up authentication with the secret
  app.configure(authentication(config));
  app.configure(jwt());
  app.configure(local());

  app.configure(oauth2(Object.assign({
    Strategy: LinkedinStrategy,
    handler: function(req, res, next) {
      OAuth2Handler(config.linkedin, req, res, next);
    }
  }, config.linkedin)));

  app.configure(oauth2(Object.assign({
    Strategy: FacebookStrategy,
    handler: function(req, res, next) {
      OAuth2Handler(config.facebook, req, res, next);
    }
  }, config.facebook)));

  app.configure(oauth2(Object.assign({
    Strategy: GithubStrategy,
    handler: function(req, res, next) {
      OAuth2Handler(config.github, req, res, next);
    }
  }, config.github)));

  // The `authentication` service is used to create a JWT.
  // The before `create` hook registers strategies that can be used
  // to create a new valid JWT (e.g. local or oauth2)
  app.service('authentication').hooks({
    before: {
      create: [
        authentication.hooks.authenticate(config.strategies)
      ],
      remove: [
        authentication.hooks.authenticate('jwt')
      ]
    }
  });

  app.on('login', (payload, { connection }) => {
    // connection can be undefined if there is no
    // real-time connection, e.g. when logging in via REST
    if (connection) {
      app.channel(`users/${connection.user._id}`).join(connection); // implicit ObjectID to string convertion of user._id
    }
  });
};
