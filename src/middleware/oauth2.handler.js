function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });
  } else {
    obj[key] = value;
  }
  return obj;
}

module.exports = function(options, req, res, next) {
  var _params, _data;

  var app = req.app;
  var authSettings = app.get('auth') || app.get('authentication') || {};
  var entity = req[authSettings.entity];
  var payload = req.payload;
  var params = (_params = {
    authenticated: true
  }, _defineProperty(_params, authSettings.entity, entity), _defineProperty(_params, 'payload', payload), _params);
  var data = (_data = {}, _defineProperty(_data, authSettings.entity, entity), _defineProperty(_data, 'payload', payload), _data);

  app.service(authSettings.path).create(data, params).then(function (result) {
    result.success = true;
    result.data = {
      entity
    };
    res.data = result;

    if (options.successRedirect) {
      res.hook = { data: {} };
      // res.hook = { data: result };
      Object.defineProperty(res.hook.data, '__redirect', { value: { status: 302, url: options.successRedirect } });
    }

    next();
  }).catch(next);
};
