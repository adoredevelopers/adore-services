const Verifier = require('@feathersjs/authentication-oauth2').Verifier;
const normalizeOAuthUserData = require('../utils/normalizeOAuthUserData');

/*
  duplicate to what the before create hook is already doing
  if the verifier does not have any other tasks then
  disable this custom verifier, the hook way is more elegant
*/
class OAuth2Verifier extends Verifier {
  _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });
    } else {
      obj[key] = value;
    }
    return obj;
  }

  _populateUserData(data) {
    return normalizeOAuthUserData(data, this.options.name);
  }

  _createEntity(data) {
    // var _entity;
    // var options = this.options;
    // var name = options.name;
    // var entity = (_entity = {}, this._defineProperty(_entity, options.idField, data.profile.id), this._defineProperty(_entity, name, data), _entity);

    // return this.service.create(entity, { oauth: { provider: name } });
    return this.service.create(this._populateUserData(data));
  }

  // The verify function has the exact same inputs and
  // return values as a vanilla passport strategy
  verify(req, accessToken, refreshToken, profile, done) {
    // do your custom stuff. You can call internal Verifier methods
    // and reference this.app and this.options. This method must be implemented.
    var _query, _this = this;

    var options = _this.options;
    var query = (_query = {}, _this._defineProperty(_query, options.providerField, options.name), _this._defineProperty(_query, options.idField, profile.id), _this._defineProperty(_query, '$limit', 1), _query);
    var data = { profile: profile, accessToken: accessToken, refreshToken: refreshToken };
    var existing = void 0;

    // Check request object for an existing entity
    if (req && req[options.entity]) {
      existing = req[options.entity];
    }

    // Check the request that came from a hook for an existing entity
    if (!existing && req && req.params && req.params[options.entity]) {
      existing = req.params[options.entity];
    }

    // If there is already an entity on the request object (ie. they are
    // already authenticated) attach the profile to the existing entity
    // because they are likely "linking" social accounts/profiles.
    if (existing) {
      return _this._updateEntity(existing, data).then(function (entity) {
        return done(null, entity);
      }).catch(function (error) {
        return error ? done(error) : done(null, error);
      });
    }

    // Find or create the user since they could have signed up via facebook.
    _this.service.find({ query: query }).then(_this._normalizeResult).then(function (entity) {
      return entity ? _this._updateEntity(entity, data) : _this._createEntity(data);
    }).then(function (entity) {
      var id = entity[_this.service.id];
      var payload = _this._defineProperty({}, options.entity + 'Id', id);
      done(null, entity, payload);
    }).catch(function (error) {
      return error ? done(error) : done(null, error);
    });

    // the second variable can be any truthy value
    // done(null, profile);
  }
}

module.exports = OAuth2Verifier;
