const axios = require('axios');

const _populateUserDataWithLinkedIn = async function(data, provider, imageStorageProvider) {
  // const jsonData = data.profile._json;
  const accessToken = data.accessToken;
  // retreive email address
  const emailAddressResp = await axios.get('https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))', {
    headers: {
      'Authorization': 'Bearer ' + accessToken
    }
  });
  // retreive profile
  const profileResp = await axios.get('https://api.linkedin.com/v2/me?projection=(id,firstName,localizedFirstName,lastName,localizedLastName,profilePicture(displayImage~:playableStreams))', {
    headers: {
      'Authorization': 'Bearer ' + accessToken
    }
  });
  let user = {};
  
  user.thirdPartyAuthProvider = provider;
  user.thirdPartyAuthId = profileResp.data.id;
  user.accountStatus = 'PENDING_USER_PROFILE';
  user.email = emailAddressResp.data.elements[0]['handle~'].emailAddress;
  // const displayImage100 = profileResp.data ? profileResp.data.profilePicture['displayImage~'].elements[0].identifiers[0].identifier : '';
  const displayImage200 = profileResp.data ? profileResp.data.profilePicture['displayImage~'].elements[1].identifiers[0].identifier : '';
  // const displayImage400 = profileResp.data ? profileResp.data.profilePicture['displayImage~'].elements[2].identifiers[0].identifier : '';
  const displayImage800 = profileResp.data ? profileResp.data.profilePicture['displayImage~'].elements[3].identifiers[0].identifier : '';
  user.displayImages = {
    [imageStorageProvider]: {
      120: displayImage200,
      640: displayImage800
    }
  };

  user.profile = {};
  user.profile.basic = {};
  user.profile.basic.name = profileResp.data.localizedFirstName + ' ' + profileResp.data.localizedLastName;

  return user;
};

const _populateUserDataWithFacebook = function(data, provider, imageStorageProvider) {
  const jsonData = data.profile._json;
  let user = {};

  user.thirdPartyAuthProvider = provider;
  user.thirdPartyAuthId = jsonData.id;
  user.accountStatus = 'PENDING_USER_PROFILE';
  user.email = jsonData.email ? jsonData.email : null;
  user.nickname = jsonData.name;
  user.displayImages = [
    {
      [imageStorageProvider]: {
        120: jsonData.picture.data.url,
        640: jsonData.picture.data.url
      }
    }
  ];
  user.profile = {};
  user.profile.basic = {};
  user.profile.basic.name = jsonData.name;
  user.profile.basic.gender = jsonData.gender != null ? jsonData.gender.charAt(0).toUpperCase() + jsonData.gender.slice(1) : null;
  user.introductions = {};
  user.introductions.introducingMyself = jsonData.bio;

  return user;
};

const _populateUserDataWithGithub = function(data, provider, imageStorageProvider) {
  const jsonData = data.profile._json;
  let user = {};

  user.thirdPartyAuthProvider = provider;
  user.thirdPartyAuthId = jsonData.id;
  user.accountStatus = 'PENDING_USER_PROFILE';
  user.email = jsonData.email ? jsonData.email : '';
  user.nickname = jsonData.name;
  user.displayImages = [
    {
      [imageStorageProvider]: {
        120: jsonData.avatar_url,
        640: jsonData.avatar_url
      }
    }
  ];
  user.profile = {};
  user.profile.basic = {};
  user.profile.basic.name = jsonData.name;
  user.profile.background = {};
  user.profile.background.company = jsonData.company;
  user.introductions = {};
  user.introductions.introducingMyself = jsonData.bio;

  return user;
};

module.exports = async function(data, provider, imageStorageProvider) {
  if (provider === 'linkedin') {
    return await _populateUserDataWithLinkedIn(data, provider, imageStorageProvider);
  }
  if (provider === 'facebook') {
    return _populateUserDataWithFacebook(data, provider, imageStorageProvider);
  }
  if (provider === 'github') {
    return _populateUserDataWithGithub(data, provider, imageStorageProvider);
  }
  return data;
};
