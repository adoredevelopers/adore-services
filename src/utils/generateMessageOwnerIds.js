module.exports = function(userId1, userId2) {
  let ownerIds = [];
  if (userId1.toString() < userId2.toString()) {
    ownerIds.push(userId1);
    ownerIds.push(userId2);
  } else {
    ownerIds.push(userId2);
    ownerIds.push(userId1);
  }
  return ownerIds;
};
