// messages-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const messages = new Schema({
    conversationId: { type: Schema.Types.ObjectId },
    senderId: { type: Schema.Types.ObjectId },
    recipientId: { type: Schema.Types.ObjectId },
    message: { type: String },
    createdAt: { type: Date, default: Date.now }
  });

  return mongooseClient.model('messages', messages);
};
