// conversations-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const conversations = new Schema({
    ownersId: { type: String }, // owners[0] + owners[1], lookup key
    owners: [{ type: mongooseClient.Schema.Types.ObjectId }], // array index contains conversation owners _id field, with natural ordering
    createdAt: { type: Date, default: Date.now }
  });

  return mongooseClient.model('conversations', conversations);
};
