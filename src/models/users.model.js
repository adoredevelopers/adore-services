// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const users = new mongooseClient.Schema({
    email: { type: String },
    password: { type: String, default: null },
    accountType: { type: String, default: 'USER' }, // USER, USER_DP_VERIFIED, USER_PREMIUM, SYSTEM, BOT, ADMIN
    accountStatus: { type: String, default: 'PENDING_EMAIL_VERIFICATION' }, // ACTIVE, PENDING_EMAIL_VERIFICATION, PENDING_USER_PROFILE, BANNED, INACTIVE
    thirdPartyAuthProvider: { type: String, default: null },
    thirdPartyAuthId: { type: String, default: null },
    /*
      displayImages: [
        {
          default: [
            120: 'http://adoreapp.love/images/avatar/userId/abcd1234_120.jpg',
            640: 'http://adoreapp.love/images/avatar/userId/abcd1234_640.jpg'
          ],
          firebase: [
            ...
          ],
          aws: [
            ...
          ]
        },
        {
          ...
        }
      ]
    */
    displayImages: [],
    /*
    loc: {
      type: { type: String, default: 'Point' },
      coordinates: [{ type: Number }] // [ longitude, latitude ]
    },
    */
    loc: {},
    profile: {
      basic: {
        name: { type: String, default: null },
        gender: { type: String, default: null },
        birthday: { type: Date, default: null },
        height: { type: Number, default: null },
        bodyFigure: { type: String, default: null },
        martialStatus: { type: String, default: null },
        hasChildren: { type: Boolean, default: null },
        languages: [{ type: String }],
        postalCode: { type: String, default: null },
        province: { type: String, default: null },
        country: { type: String, default: null },
        city: { type: String, default: null },
        residentialStatus: { type: String, default: null }, 
        willingToRelocate: { type: Boolean, default: null }
      },
      background: {
        highestEducation: { type: String, default: null },
        educationInstitute: [{ type: String }],
        fieldOfStudy: { type: String, default: null },
        industry: { type: String, default: null },
        company: { type: String, default: null },
        jobTitle: { type: String, default: null },
        income: { type: String, default: null },
        religion: { type: String, default: null },
        ethnicities: [{ type: String }]
      },
      lifestyle: {
        smoking: { type: String, default: null },
        drinking: { type: String, default: null },
        placesTraveled: [{ type: String }],
        interests: [{ type: String }],
        books: [{ type: String }],
        music: [{ type: String }],
        moviesAndShows: [{ type: String }]
      }
    },
    introductions: {
      introducingMyself: { type: String, default: '' },
      howISpendFreeTime: { type: String, default: '' },
      peoplesFirstImpressionOfMe: { type: String, default: '' },
      howFriendsDiscribeMe: { type: String, default: '' },
      topicsILoveToDiscuss: { type: String, default: '' },
      whatIEnjoyInLife: { type: String, default: '' },
      qualitiesIAdore: { type: String, default: '' },
    },
    preference: {
      gender: [{ type: String }],
      minAge: { type: Number, default: null },
      maxAge: { type: Number, default: null },
      minHeight: { type: Number, default: null },
      maxHeight: { type: Number, default: null },
      martialStatus: [{ type: String }],
      hasChildren: [{ type: Boolean }],
      
      languages: [{ type: String }],
      education: [{ type: String }],
      income: { type: Number, default: null },
      religions: [{ type: String }],
      ethnicities: [{ type: String }],
      
      smoking: [{ type: String }],
      drinking: [{ type: String }],
    },
    stats: {
      admirers: { type: Number, default: 0 }, 
      adorers: { type: Number, default: 0 }, 
      visits: { type: Number, default: 0 }
    },
    misc: {
      unreadBadgeCount: { 'type': Number, 'default': 0 },
      resetPassword: {
        authorizationToken: { type: String, default: null },
        expirationDate: { type: Date, default: null }
      },
      report: [{
        reporter: { type: String },
        reason: { type: String },
        imageUrl: { type: String },
        comment: { type: String },
        reportDate: { type: Date }
      }]
    },
    /* might want to extract to other collections */
    // visitedUsers: [{ type: mongooseClient.Schema.Types.ObjectId }],
    // visitingUsers: [{ type: mongooseClient.Schema.Types.ObjectId }],
    // adoredUsers: [{ type: mongooseClient.Schema.Types.ObjectId }],
    // adoringUsers: [{ type: mongooseClient.Schema.Types.ObjectId }],
    // removedMessageUsers: [{ type: mongooseClient.Schema.Types.ObjectId }],
    // blockedUsers: [{ type: mongooseClient.Schema.Types.ObjectId }],
    lastActiveAt: { type: Date, default: Date.now },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
  });

  return mongooseClient.model('users', users);
};
