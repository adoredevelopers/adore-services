'use strict';

const errors = require('@feathersjs/errors');

module.exports = function() {
  return async function(hook) {
    let conversations = hook.result.data;
    let actionUserIdString = hook.params.user._id.toString();
    let unAuthorizedAction = false;
    for (let i = 0; i < conversations.length; i++) {
      if (conversations[i].owners[0].toString() !== actionUserIdString && conversations[i].owners[1].toString() !== actionUserIdString) {
        unAuthorizedAction = true;
        break;
      }
    }
    if (hook.params.user.isAdmin || !unAuthorizedAction) {
      const findConversationMessagesPromises = conversations.map(function(conversation) {
        let params = {
          query: {
            conversationId: conversation._id,
            $sort: {
              createdAt: -1
            },
            $limit: 20
          }
        };
        if (hook.params.beforeCreatedAt) {
          params.query.createdAt = {
            $lt: hook.params.beforeCreatedAt
          };
        }
        return hook.app.service('messages').find(params);
      });
      await Promise.all(findConversationMessagesPromises).then(function(resultArray) {
        resultArray.forEach(function(result, idx) {
          conversations[idx].messages = result.data.reverse();
        });
      });
    } else {
      hook.error = new errors.Forbidden('User is not authorized to perform this action.');
    }
    
    return Promise.resolve(hook);
  };
};