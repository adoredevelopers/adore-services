'use strict';

module.exports = function() {
  return function(hook) {
    if (hook.params.query != null && hook.params.query.beforeCreatedAt != null) {
      hook.params.beforeCreatedAt = hook.params.query.beforeCreatedAt;
      delete hook.params.query.beforeCreatedAt;
    }
    
    return Promise.resolve(hook);
  };
};