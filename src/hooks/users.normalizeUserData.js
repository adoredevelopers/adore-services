'use strict';

const normalizeOAuthUserData = require('../utils/normalizeOAuthUserData');

module.exports = function() {
  return async function(hook) {
    // execute hook only when user data coming from oauth provider
    if (hook.params.oauth) {
      hook.data = await normalizeOAuthUserData(hook.data[hook.params.oauth.provider], hook.params.oauth.provider, hook.app.get('imageStorage'));
    } else {
      console.debug(hook.data);
    }

    return Promise.resolve(hook);
  };
};
