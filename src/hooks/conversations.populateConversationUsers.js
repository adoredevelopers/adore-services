'use strict';

module.exports = function() {
  return function(hook) {
    const conversations = hook.result.data;
    // for each conversation get the conversation other user and
    // construct an array of user ids to fetch the user in batch
    // then later insert each user found into corresponding conversation
    const conversationUsers = conversations.map((conversation) => {
      for (let i = 0; i < conversation.owners.length; i++) {
        if (conversation.owners[i].toString() !== hook.params.user._id.toString()) return conversation.owners[i];
      }
    });
    return hook.app.service('users').find({
      query: {
        _id: {
          $in: conversationUsers
        }
      }
    }).then((result) => {
      result.data.forEach((user) => {
        const conversationUsersStrings = conversationUsers.map(userId => userId.toString());
        const conversationIndex = conversationUsersStrings.indexOf(user._id.toString());
        conversations[conversationIndex].messageUser = user;
      });
    }, (err) => {
      hook.app.error(err);
      return Promise.resolve(hook);
    });
  };
};