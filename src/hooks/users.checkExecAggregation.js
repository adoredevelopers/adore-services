'use strict';

module.exports = function() {
  return function(hook) {
    let sort = hook.params.query.$sort;
    delete hook.params.query.$sort;
    if (sort && sort.distance) { //geolocation distance sorting, intercepting and using aggregation
      const conditions = [
        {
          $geoNear: {
            query: hook.params.query,
            near: {
              type: 'Point',
              coordinates: [-81.093699, 32.074673]
            }, 
            /*minDistance: 0,
            maxDistance: 500000,*/
            spherical: true,
            distanceField: 'distance',
            limit: 1000 // hard limit to how many users should be returned when sorting base on distance
          }
        },
        {
          $skip: sort.skip
        },
        {
          $group: {
            _id: null,
            total: {
              $sum: 1 
            },
            results: {
              $push: '$$ROOT'
            }
          }
        }
      ];
      return new Promise(function(resolve, reject) {
        hook.app.service('users').Model.aggregate(conditions).exec(function(error, result) {
          if (error) {
            hook.app.error(error);
            return reject(error);
          }
          if (result[0]) {
            result = result[0];
            result.data = result.results.slice(0, hook.app.get('paginate').default);
            result.skip = sort.skip;
            result.limit = hook.app.get('paginate').default;
          } else {
            result.data = [];
            result.skip = 0;
            result.limit = 10;
          }
          delete result._id;
          delete result.results;
          hook.result = result;
          return resolve(hook);
        });
      });
    }
  };
};
