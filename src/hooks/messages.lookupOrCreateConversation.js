'use strict';

const generateMessageOwnerIds = require('../utils/generateMessageOwnerIds');

module.exports = function() {
  return function(hook) {
    let owners = generateMessageOwnerIds(hook.data.senderId, hook.data.recipientId);
    let ownersId = owners[0] + owners[1];
    return hook.app.service('conversations').find({
      query: {
        owners: owners
      },
      user: {
        _id: hook.data.senderId
      }
    }).then((result) => {
      if (result == null || result.data == null || result.data.length === 0) {
        return hook.app.service('conversations').create({
          ownersId,
          owners
        }).then((conversation) => {
          hook.data.conversationId = conversation._id;
          return Promise.resolve(hook);
        });
      } else {
        hook.data.conversationId = result.data[0]._id;
        return Promise.resolve(hook);
      }
    });
  };
};
