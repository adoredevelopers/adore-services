const { authenticate } = require('@feathersjs/authentication').hooks;
const { disallow } = require('feathers-hooks-common');

const lookupOrCreateConversation = require('../../hooks/messages.lookupOrCreateConversation');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [ disallow('external') ], // allow calls from server only
    get: [ disallow('external') ],
    create: [ lookupOrCreateConversation() ],
    update: [ disallow() ],
    patch: [ disallow() ],
    remove: [ disallow() ]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
