const { authenticate } = require('@feathersjs/authentication').hooks;
const { restrictToOwner } = require('feathers-authentication-hooks');
const restrict = [
  authenticate('jwt'),
  restrictToOwner({
    idField: '_id',
    ownerField: 'owners'
  })
];

const populateConversationUsers = require('../../hooks/conversations.populateConversationUsers');
const populateConversationMessages = require('../../hooks/conversations.populateConversationMessages');
const forwardParams = require('../../hooks/conversations.forwardParams');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [ forwardParams() ],
    get: [ ...restrict ],
    create: [],
    update: [ ...restrict ],
    patch: [ ...restrict ],
    remove: [ ...restrict ]
  },

  after: {
    all: [],
    find: [ populateConversationUsers(), populateConversationMessages() ],
    get: [ populateConversationUsers(), populateConversationMessages() ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
