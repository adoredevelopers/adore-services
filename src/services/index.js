const users = require('./users/users.service.js');
const messages = require('./messages/messages.service.js');
const conversations = require('./conversations/conversations.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(users);
  app.configure(messages);
  app.configure(conversations);
};
