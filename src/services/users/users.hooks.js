const { authenticate } = require('@feathersjs/authentication').hooks;
const { restrictToOwner } = require('feathers-authentication-hooks');
const { hashPassword, protect } = require('@feathersjs/authentication-local').hooks;
const restrict = [
  authenticate('jwt'),
  restrictToOwner({
    idField: '_id',
    ownerField: '_id'
  })
];

const checkExecAggregation = require('../../hooks/users.checkExecAggregation');
const normalizeUserData = require('../../hooks/users.normalizeUserData');

module.exports = {
  before: {
    all: [],
    find: [
      checkExecAggregation()
    ],
    get: [
      authenticate(['jwt'])
    ],
    create: [
      normalizeUserData(),
      hashPassword()
    ],
    update: [
      ...restrict,
      hashPassword()
    ],
    patch: [
      ...restrict,
      hashPassword()
    ],
    remove: [
      ...restrict
    ]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
