FROM node:13.8.0

ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin
ENV YARN_VERSION 1.22.0

RUN curl -fSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz" \
    && tar -xzf yarn-v$YARN_VERSION.tar.gz -C /opt/ \
    && ln -snf /opt/yarn-v$YARN_VERSION/bin/yarn /usr/local/bin/yarn \
    && ln -snf /opt/yarn-v$YARN_VERSION/bin/yarnpkg /usr/local/bin/yarnpkg \
    && rm yarn-v$YARN_VERSION.tar.gz

EXPOSE 3030

USER node

WORKDIR /home/node

COPY --chown=node:node package.json .

COPY --chown=node:node yarn.lock .

RUN yarn install --pure-lockfile --network-timeout 100000

COPY --chown=node:node . .

CMD ["node", "src/"]